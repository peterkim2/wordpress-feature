$(document).ready(function() {

var textarea = $('#text-area');

  $('p').on('dragstart', function (event) {
    var selectedButton = $(this);
    event.originalEvent.dataTransfer.setData('Text', '<' + selectedButton.attr('data') + '>' + '</' + selectedButton.attr('data') + '>');
    textarea.focus();
    textarea.selectionEnd = 2;
    event.originalEvent.dataTransfer.setDragImage(dragIcon, -10, -10);
  });

  $('#buttons p').on('click', function() {
    var selectedButton = $(this);
    var insertBox = selectedButton.next();
    var a = '<' + selectedButton.attr('data') + '>';
    var i = '</' + selectedButton.attr('data') + '>';
    if(insertBox.val() != "") {
      textarea.val(textarea.val() + '<' + selectedButton.attr('data') + '>' + insertBox.val() + '</' + selectedButton.attr('data') + '>');
      insertBox.val('');
      textarea.focus();
      textarea.prop('selectionEnd', (textarea.val()).length - i.length);
    } else {
      var start = textarea.prop('selectionStart');
      var end = textarea.prop('selectionEnd');
      var sel = textarea.val().substring(start, end);
      var finText = textarea.val().substring(0, start) + a + sel + i + textarea.val().substring(end);
      textarea.val(finText);
      textarea.focus();
      textarea.prop('selectionEnd', end + a.length);
    }
  });

});

var dragIcon = new Image();
dragIcon.src = ' ';
dragIcon.width = 100;

function eraseText() {
  document.getElementById('text-area').value = "";
}
