'use strict';

const express = require('express');
const PORT = process.env.PORT || 8000;
const app = express();

app.use(express.static('./'));

app.get('*', function(request, response) {
  console.log('New request:', request.url);
  response.sendFile('index.html', {root: '.'});
});

app.listen(PORT, () => {
  console.log(`Server up: ${PORT}`);
});
